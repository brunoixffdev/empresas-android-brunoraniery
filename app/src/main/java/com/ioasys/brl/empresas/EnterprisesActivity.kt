package com.ioasys.brl.empresas

import android.app.SearchManager
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import com.ioasys.brl.empresas.adapters.EnterpriseItemAdapter
import com.ioasys.brl.empresas.databinding.ActivityEnterprisesBinding
import com.ioasys.brl.empresas.helpers.CredentialsManager
import com.ioasys.brl.empresas.helpers.RetrofitInitializer
import com.ioasys.brl.empresas.listeners.OnItemClickListener
import com.ioasys.brl.empresas.models.Enterprise
import com.ioasys.brl.empresas.models.EnterpriseList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EnterprisesActivity : AppCompatActivity() {

    lateinit var binding: ActivityEnterprisesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEnterprisesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)



    }

    private fun loadEnterprises(query: String?){
        binding.progressEnterprises.visibility = View.VISIBLE
        val credentials = CredentialsManager.getCredentials(this@EnterprisesActivity)
        val call = RetrofitInitializer().enterprisesService().enterprises(credentials.accessToken,credentials.client,credentials.uid,query)
        call.enqueue(object: Callback<EnterpriseList> {
            override fun onResponse(call: Call<EnterpriseList>, response: Response<EnterpriseList>) {
                when(response.code()){
                    200 -> {
                        response.body()?.let{
                            val enterprises: List<Enterprise> = it.enterprises
                            if(enterprises.isNotEmpty()){
                                binding.tvNoResults.visibility = View.GONE
                            }else{
                                binding.tvNoResults.visibility = View.VISIBLE
                            }
                            configureList(enterprises)
                        }
                    }
                    else -> {
                        Log.i("response error", response.message().toString())
                        binding.tvNoResults.visibility = View.VISIBLE
                    }
                }
                binding.progressEnterprises.visibility = View.GONE
            }

            override fun onFailure(call: Call<EnterpriseList>, t: Throwable) {
                Log.e("onFailure error", t.message.toString())
                binding.tvNoResults.visibility = View.VISIBLE
                binding.progressEnterprises.visibility = View.GONE

            }

        })

    }

    private fun configureList(enterprises: List<Enterprise>) {
        binding.recyclerView.adapter = EnterpriseItemAdapter(this,enterprises,
            object : OnItemClickListener {
                override fun onItemClicked(enterprise: Enterprise) {
                    val intent = Intent(this@EnterprisesActivity,EnterpriseDetailActivity::class.java)
                    intent.putExtra("EnterpriseId", enterprise.id)
                    startActivity(intent)
                }
            })
        binding.recyclerView.setHasFixedSize(true)


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)

        val expandListener = object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                binding.ivLogoMenu.visibility = View.VISIBLE
                return true
            }

            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                binding.ivLogoMenu.visibility = View.GONE
                binding.tvClickOnSearch.visibility = View.GONE
                item.actionView
                return true
            }
        }

        val manager = getSystemService(SEARCH_SERVICE) as SearchManager
        val actionMenuItem = menu?.findItem(R.id.action_search)
        val searchView = actionMenuItem?.actionView as SearchView


        searchView.setSearchableInfo(manager.getSearchableInfo(componentName))
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                loadEnterprises(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })

        actionMenuItem?.actionView?.background?.alpha = 0

        actionMenuItem?.setOnActionExpandListener(expandListener)



        return true

    }
}