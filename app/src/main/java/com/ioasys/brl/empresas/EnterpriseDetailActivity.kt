package com.ioasys.brl.empresas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.ioasys.brl.empresas.databinding.ActivityEnterpriseDetailBinding
import com.ioasys.brl.empresas.helpers.CredentialsManager
import com.ioasys.brl.empresas.helpers.RetrofitInitializer
import com.ioasys.brl.empresas.models.Enterprise
import com.ioasys.brl.empresas.models.EnterpriseDetail
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EnterpriseDetailActivity : AppCompatActivity() {

    lateinit var binding: ActivityEnterpriseDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEnterpriseDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)


        if(intent.hasExtra("EnterpriseId")){
            val enterpriseId = intent.getIntExtra("EnterpriseId",0)

            val credentials = CredentialsManager.getCredentials(this@EnterpriseDetailActivity)
            binding.progressEnterprise.visibility = View.VISIBLE
            val call = RetrofitInitializer().enterprisesService().enterprise(credentials.accessToken,credentials.client,credentials.uid,enterpriseId)
            call.enqueue(object: Callback<EnterpriseDetail> {
                override fun onResponse(call: Call<EnterpriseDetail>, response: Response<EnterpriseDetail>) {
                    when(response.code()){
                        200 -> {
                            response.body()?.let{
                                val enterprise: Enterprise = it.enterprise
                                binding.tvEnterpriseDetail.text = enterprise.description
                                Glide.with(this@EnterpriseDetailActivity)
                                    .load(BuildConfig.BASE_URL.plus(enterprise.photo))
                                    .into(binding.ivEnterpriseImageDetail)
                                binding.toolbar.title = enterprise.enterpriseName
                            }
                        }
                        else -> {
                            Log.i("response error", response.message().toString())
                        }
                    }
                    binding.progressEnterprise.visibility = View.GONE
                }

                override fun onFailure(call: Call<EnterpriseDetail>, t: Throwable) {
                    Log.e("onFailure error", t.message.toString())
                    binding.progressEnterprise.visibility = View.GONE

                }

            })
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}