package com.ioasys.brl.empresas.listeners

import com.ioasys.brl.empresas.models.Enterprise

interface OnItemClickListener {

    fun onItemClicked(enterprise: Enterprise)
}