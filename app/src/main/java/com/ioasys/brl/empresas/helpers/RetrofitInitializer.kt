package com.ioasys.brl.empresas.helpers

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ioasys.brl.empresas.BuildConfig
import com.ioasys.brl.empresas.services.EnterpriseService
import com.ioasys.brl.empresas.services.LoginService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInitializer {

    private val gson:Gson = GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create()

    private val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

    fun loginService() : LoginService{
        return retrofit.create(LoginService::class.java)
    }
    fun enterprisesService() : EnterpriseService{
        return retrofit.create(EnterpriseService::class.java)
    }
}