package com.ioasys.brl.empresas.helpers

import android.content.Context
import com.google.gson.GsonBuilder
import com.ioasys.brl.empresas.models.Credentials

object CredentialsManager {

    private const val PREFERENCES_KEY = "OAuth2"
    private const val AUTHENTICATION = "authentication"

    fun saveCredentials(context: Context, credentials: Credentials){
        val credentialsJson = GsonBuilder().create().toJson(credentials)
        val sharedPreferences = context.getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE)
        sharedPreferences!!.edit().putString(AUTHENTICATION, credentialsJson).apply()
    }

    fun getCredentials(context: Context):Credentials{
        val sharedPreferences = context.getSharedPreferences(PREFERENCES_KEY,Context.MODE_PRIVATE)
        val credentialsJson = sharedPreferences!!.getString(AUTHENTICATION,"")
        return GsonBuilder().create().fromJson(credentialsJson, Credentials::class.java)
    }
}