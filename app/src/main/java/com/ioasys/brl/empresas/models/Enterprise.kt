package com.ioasys.brl.empresas.models

data class Enterprise(val id:Int,
                      val enterpriseName:String,
                      val photo: String,
                      val country: String,
                      val description:String,
                      val enterpriseType: EnterpriseType)

data class EnterpriseType (val enterpriseTypeName: String)

data class EnterpriseList (val enterprises: List<Enterprise>)

data class EnterpriseDetail (val enterprise: Enterprise)
