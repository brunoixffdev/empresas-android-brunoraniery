package com.ioasys.brl.empresas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.ioasys.brl.empresas.databinding.ActivityLoginBinding
import com.ioasys.brl.empresas.helpers.CredentialsManager
import com.ioasys.brl.empresas.helpers.RetrofitInitializer
import com.ioasys.brl.empresas.models.Credentials
import com.ioasys.brl.empresas.models.Login
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    lateinit var binding: ActivityLoginBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnEnter.setOnClickListener {
            val email = binding.editEmail.text.toString()
            val password = binding.editPassword.text.toString()
            val call = RetrofitInitializer().loginService().login(Login(email, password))
            binding.progressLayout.visibility = View.VISIBLE
            call.enqueue(object: Callback<ResponseBody>{
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    binding.progressLayout.visibility = View.GONE
                    response.headers()?.let{
                        when(response.code()){
                        200 -> {
                            CredentialsManager.saveCredentials(this@LoginActivity, Credentials(
                                    response.headers().get("access-token"),
                                    response.headers().get("client"),
                                    response.headers().get("uid")

                            ))

                            val intent = Intent(this@LoginActivity,EnterprisesActivity::class.java)
                            startActivity(intent)

                            }
                        else -> {
                            binding.editEmail.error = getString(R.string.login_error)
                            binding.containerEditPassword.error = getString(R.string.login_error)

                        }
                        }

                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.e("onFailure error", t.message.toString())
                    binding.progressLayout.visibility = View.GONE
                }

            })

        }



    }

}