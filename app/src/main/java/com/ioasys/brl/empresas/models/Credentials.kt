package com.ioasys.brl.empresas.models

data class Credentials(val accessToken: String?, val client: String?, val uid: String?)
