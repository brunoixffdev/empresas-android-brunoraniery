package com.ioasys.brl.empresas.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ioasys.brl.empresas.BuildConfig
import com.ioasys.brl.empresas.R
import com.ioasys.brl.empresas.listeners.OnItemClickListener
import com.ioasys.brl.empresas.models.Enterprise

class EnterpriseItemAdapter(private val context: Context,
                            private val dataset:List<Enterprise>,
                            private val itemClickListener: OnItemClickListener
                            ):RecyclerView.Adapter<EnterpriseItemAdapter.EnterpriseItemViewHolder>() {
    class EnterpriseItemViewHolder(private val view: View):RecyclerView.ViewHolder(view) {
        val ivEnterpriseLogo: ImageView = view.findViewById(R.id.iv_enterprise_image)
        val tvEnterpriseName: TextView = view.findViewById(R.id.tv_enterprise_name)
        val tvEnterpriseCountry: TextView = view.findViewById(R.id.tv_enterprise_country)
        val tvEnterpriseType: TextView = view.findViewById(R.id.tv_enterprise_type)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EnterpriseItemViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
                .inflate(R.layout.enterprise_list_item, parent, false)

        return EnterpriseItemViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: EnterpriseItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.tvEnterpriseName.text = item.enterpriseName
        holder.tvEnterpriseCountry.text = item.country
        holder.tvEnterpriseType.text = item.enterpriseType.enterpriseTypeName
        Glide.with(context)
            .load(BuildConfig.BASE_URL.plus(item.photo))
            .into(holder.ivEnterpriseLogo)
        holder.itemView.setOnClickListener{
            itemClickListener.onItemClicked(item)
        }
    }

    override fun getItemCount(): Int {
        return dataset.size
    }
}