package com.ioasys.brl.empresas.services

import com.ioasys.brl.empresas.BuildConfig
import com.ioasys.brl.empresas.models.EnterpriseDetail
import com.ioasys.brl.empresas.models.EnterpriseList
import retrofit2.Call
import retrofit2.http.*

interface EnterpriseService {

    @GET("api/${BuildConfig.API_VERSION}/enterprises")
    fun enterprises(@Header("access-token") accessToken : String?,
                    @Header("client") client : String?,
                    @Header("uid") uid : String?,
                    @Query("name") query: String?): Call<EnterpriseList>

    @GET("api/${BuildConfig.API_VERSION}/enterprises/{id}")
    fun enterprise(@Header("access-token") accessToken : String?,
                    @Header("client") client : String?,
                    @Header("uid") uid : String?,
                    @Path("id") id: Int): Call<EnterpriseDetail>



}