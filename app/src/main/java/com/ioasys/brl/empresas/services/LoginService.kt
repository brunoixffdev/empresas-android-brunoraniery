package com.ioasys.brl.empresas.services

import com.ioasys.brl.empresas.BuildConfig
import com.ioasys.brl.empresas.models.Login
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface LoginService {

    @Headers("Content-Type:application/json")
    @POST("api/${BuildConfig.API_VERSION}/users/auth/sign_in")
    fun login(@Body login: Login):Call<ResponseBody>
}